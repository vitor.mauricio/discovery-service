#!/bin/bash
#
#	Script para disparar o discovery service
#

# Cores do terminal
YELLOW="\x1b[33;1m"
BLUE="\x1b[34;1m"
RED="\x1b[31;1m"
GREEN="\x1b[32;1m"
RESET="\x1b[0m"
MAGENTA="\x1b[35;1m"
CYAN="\x1b[36;1m"

# Parâmetros
id_param="--id"
path_param="--path"
dhandler_param="--ip"

# Valores padrões
instance_id="09727695-0d37-4eef-9280-f8934b475304"
path="00000000-0000-0000-0000-000000000000"
data_handler_ip="10.101.34.121"

# Exibe mensagem de sucesso na tela
function log_ok()
{
    echo -e "$GREEN[+]$RESET $1"
}

# Exibe mensagem de erro na tela
function log_error()
{
    echo -e "$RED[!]$RESET $1"
}

# Exibe mensagem de informação na tela
function log_info()
{
    echo -e "$BLUE[*]$RESET $1"
}

# Exibe uma mensagem de alerta na tela
function log_warning()
{
    echo -e "$YELLOW[!]$RESET $1"
}

# Exibe uma mensagem de erro e fecha o programa
function panic()
{
    log_error "$1\n"
    exit 1
}

function usage()
{
	echo ""
	echo "Usage: $0 $id_param <id> $path_param <path> $dhandler_param <data-handler-ip>"
    echo ""
	echo -e "<id>: instance ID\t\t\t\t (Default: $instance_id)"
    echo -e "<path>: path to http url\t\t\t (Default: $path)"
    echo -e "<data-handler-ip>: ipv4 address of data handler\t (Default: $data_handler_ip)"
    echo ""
    exit 1
}

function parse_arguments()
{
	if [[ "$1" != "$id_param" || "$3" != "$path_param" || "$5" != "$dhandler_param" ]]; then
		echo "Failed to parse arguments"
        echo -e "The program will continue with default values ...\n"
	else
        if ["$1" == "-h" || "$1" == "--help"]; then
            usage $0
        fi

        instance_id="$2"
        path="$4"
        data_handler_ip="$6"

        log_info "IP: $data_handler_ip, Path: $path, Id: $instance_id"
    fi
}

function main()
{
	curl --verbose -H "Content-Type: application/json" --data "{\"instance_id\":\"$instance_id\"}" http://$data_handler_ip:9090/discovery/$path
}

argc=("$#")

if [[ "$1" == "-h" || "$1" == "--help" && "$argc" == "1" ]]; then
    usage $0
fi

parse_arguments $1 $2 $3 $4 $5 $6
main
