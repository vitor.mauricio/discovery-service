package senders

import (
	"bytes"
	"discovery-service/settings"
	"errors"
	"io/ioutil"
	"net/http"
	"time"
)

//DHSender is the variables that contains a Sender Client
var DHSender *DataHandlerClient

//DataHandlerClient is a client that will make Data Handler's request
type DataHandlerClient struct {
	path   map[string]string
	method map[string]string
	client *http.Client
}

//NewDataHandlerClient creates a new sender Client
func NewDataHandlerClient() (s *DataHandlerClient) {
	return &DataHandlerClient{

		//Sets a IP with path to Data Handler for your specific operation
		path: map[string]string{
			settings.OperationObject:    settings.DataHandlerHostPort + settings.DataHandlerPathObject,
			settings.OperationDiscovery: settings.DataHandlerHostPort + settings.DataHandlerPathDiscovery + settings.Slash + settings.DiscoveryID,
			settings.OperationDone:      settings.DataHandlerHostPort + settings.DataHandlerPathDiscovery + settings.Slash + settings.DiscoveryID + settings.DataHandlerPathDone,
		},

		//Sets a method (on HTTP request) for each operation
		method: map[string]string{
			settings.OperationObject:    http.MethodPost,
			settings.OperationDiscovery: http.MethodGet,
			settings.OperationDone:      http.MethodPost,
		},

		//Define this attribute as an HTTP client
		client: &http.Client{},
	}
}

//Send sends a request to data handler with specific operation
func (s *DataHandlerClient) Send(payload string, operation string, requestID string) (payloadResponse []byte, err error) {

	//Set a timeout for datahandler client connection
	s.client.Timeout = settings.DataHandlerTimeout * time.Second

	//Create a bytes buffer
	var b bytes.Buffer

	//Write the payload on buffer
	b.Write([]byte(payload))

	//Create a new request with the method, url and body (bytes buffer)
	req, err := http.NewRequest(s.method[operation], s.path[operation], &b)

	//If there is an error return it
	if err != nil {
		return nil, err
	}

	//Add Content-Type header to request
	req.Header.Add("Content-Type", "application/json")

	//If the operation is to create a scenario, will be added another Header
	if operation == settings.OperationObject {
		req.Header.Add("Request-ID", requestID)
	}

	//Make the request effectively (as client)
	res, err := s.client.Do(req)

	//If there is an error with connection return it
	if err != nil {
		return nil, errors.New("data handler connection error :" + err.Error())
	}

	//Assure that body response will be closed at end of function
	defer res.Body.Close()

	//Parse data handler response and return an parsed Struct
	payloadResponse, err = s.retriveDataHandlerPayload(res, operation)

	//If there is an error return it
	if err != nil {
		return nil, err
	}

	return
}

//retriveDataHandlerPayload will catch data handler response payload, and will return it
func (s *DataHandlerClient) retriveDataHandlerPayload(res *http.Response, operation string) (response []byte, err error) {

	//If there is an error status code, return an error
	if res.StatusCode != http.StatusOK {
		return nil, errors.New("data handler response error : " + res.Status)
	}

	//If data handler request operation is for end of scan notify, return no error
	if operation == "done" {
		return nil, err
	}

	//Read response body and put it on a new variable
	response, err = ioutil.ReadAll(res.Body)

	//If there is an error return it
	if err != nil {
		return nil, err
	}

	return response, nil

}
