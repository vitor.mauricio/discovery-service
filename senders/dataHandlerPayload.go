package senders

import (
	"discovery-service/settings"
	"encoding/json"
)

//RegisteredScenarios is a struct that contains created scenario(s) ID(s) and will be unmarshaled
type RegisteredScenarios struct {
	ScenariosIDs json.RawMessage
}

//Create and return the struct that will be unmarshaled
func createRegisteredScenarios() *RegisteredScenarios {
	return &RegisteredScenarios{}
}

func (r *RegisteredScenarios) validate() error {
	return nil
}

//RequestID is a struct that will be marshaled
type RequestID struct {
	RequestID string `json:"request_id" validate:"uuid4"`
}

//OrderPayload is a struct that will be unmarshaled
type OrderPayload struct {
	InstanceID string `json:"instance_id"`
	RequestID  string `json:"request_id"`
}

//CreateOrderPayload creates and return the struct that will be unmarshaled
func CreateOrderPayload() *OrderPayload {
	return &OrderPayload{}
}

//Validate is a function that validade struct fields and return a possible error
func (s OrderPayload) Validate() error {
	return settings.Val.Struct(s)
}
