package senders

import (
	"discovery-service/datapackage"
	"discovery-service/settings"
	"net"
	"time"
)

//DvSender is a variable that contains an Device Client (Device Sender)
var DvSender *DeviceClient

//DeviceClient is a struct that defines what is a DeviceClient
type DeviceClient struct {
	//Sets a timeout for the TCP connection
	TCPTimeout time.Duration
}

//NewDeviceClient returns a new pointer of Device Client
func NewDeviceClient() *DeviceClient {
	return &DeviceClient{TCPTimeout: settings.TCPTimeout}
}

//Send defines an algorithm to make connections with device
func (d *DeviceClient) Send(operation string, ip string, port string, scenarios []datapackage.DataPackage) (payload []byte, err error) {

	//Sets 4 attempts to connect with device
	for i := 0; i <= 4; i++ {

		//Tries to connect with device and retrive device's payload
		payload, err = makeConn(operation, ip, port, scenarios)

		//If there is an error with connection, and it's not from Timeout, then try again
		if err != nil {
			if te, ok := err.(interface{ Timeout() bool }); ok {
				if te.Timeout() {
					return nil, err
				}
				time.Sleep(time.Millisecond * 200)
				//log.Printf("%v - IP: %v - ERROR: %v", i, ip, err)
				continue
			}
		}
		break
	}

	return
}

// makeConn will make a connection and get a SI(Payload) from host or send the
// registered scenario(s) ID
func makeConn(operation string, ip string, port string, scenarios []datapackage.DataPackage) (payload []byte, err error) {

	//Sets a timeout for the TCP connection
	timeout := settings.TCPTimeout * time.Second

	// Try to connect in TCP dial with the address resolved and a timeout
	conn, err := net.DialTimeout("tcp", net.JoinHostPort(ip, settings.DevicePort), timeout)

	// If there was an error, return it
	if err != nil {
		return nil, err
	}

	//Sets a Deadline with the same time as the connection timeout
	conn.SetDeadline(time.Now().Add(timeout))

	// Ensure connection will be closed at the end of function
	defer conn.Close()

	// If the operation is to send back the Scenario(s) ID(s)
	if operation == settings.OperationReturnScenarios {

		//Return payload that constains scenarios to device
		err := returnScenarios(scenarios, conn, operation)

		return nil, err
	}

	//Retrive device SI through device-discovery TCP connetion
	payload, err = retriveDevicePayload(scenarios, conn, operation)

	return

}

//Return to device a payload that contains created scenario(s)
func returnScenarios(scenarios []datapackage.DataPackage, conn net.Conn, operation string) (err error) {
	var devolution string

	//Concatenates specific scenarios payload into a single string (In this case, scenario code with scenario ID)
	for _, scenario := range scenarios {
		devolution += scenario.ReturnScenarioID()
	}

	//Try to write on the connection (Retriving to device)
	_, err = conn.Write([]byte(operation + devolution))

	return err
}

//Retrives device SI through device-discovery TCP connetion
func retriveDevicePayload(scenarios []datapackage.DataPackage, conn net.Conn, operation string) (payload []byte, err error) {

	// Send the operation for the host
	conn.Write([]byte(operation))

	// Create a buffer to read payload from host
	payload = make([]byte, settings.TCPResponsePayloadSize)

	// Read from connection into payload
	size, err := conn.Read(payload)

	// If there was an error, return it
	if err != nil {
		return nil, err
	}

	// Trim the payload to take only used part of buffer
	payload = payload[:size]

	// Return payload
	return
}
