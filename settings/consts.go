package settings

//DiscoveryID is the ID for discovery that middleware will use to indentificate it
var DiscoveryID string = NilUUID

//Discovery Configs
const (
	DiscoveryPollingTime = 3  // In Seconds
	DiscoveryChipIDSize  = 17 // In bytes
)

// Common Strings
const (
	EmptyString = ""
	Dash        = "-"
	Slash       = "/"
	Colon       = ":"
	Comma       = ","
	Ellipsis    = "..."
	Stdout      = "stdout"
	NilUUID     = "00000000-0000-0000-0000-000000000000"
)

//Device Manager
const (
	ConnectionSocketSize     = 1000
	DeviceManagerQueueSize   = 30
	OperationIdentify        = "/identify"
	OperationReturnScenarios = "/scenarios"
	DevicePort               = "8090"
	NetworkScanRange         = 3 // This number will be multiplied with 2 and 255

	//TCP connection
	TCPTimeout             = 10   //In seconds
	TCPResponsePayloadSize = 2048 // In Bytes
)

//Data Manager
const (
	DataManagerQueueSize = 30
	OperationObject      = "object"
)

//Data Handler Sender
const (
	DataHandlerPort          = "9090"
	DataHandlerHostPort      = "http://172.31.99.14" + Colon + DataHandlerPort
	DataHandlerPathObject    = "/object"
	DataHandlerPathDiscovery = "/discovery"
	DataHandlerPathDone      = "/done"
	DataHandlerTimeout       = 10 // In seconds
)

//Order Manager
const (
	OperationDiscovery = "discovery"
	OperationDone      = "done"
)
