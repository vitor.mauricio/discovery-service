package settings

import (
	"gopkg.in/go-playground/validator.v9"
)

//Val is a validator instance to validate specif fields
var Val = validator.New()
