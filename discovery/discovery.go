package discovery

import (
	"discovery-service/managers"
	"discovery-service/senders"
	"log"
	"net"
)

//Start will start Discovery Service
func Start() {

	//Load flags
	//settings.LoadFlags()

	//Start Senders
	startSenders()

	//Start device and data manager and start a new user (retriving his IP)
	startManagers(startUser())

}

//startUser Verify if the user IP is catchable and return it
func startUser() (userIP string) {

	//Figure out user IP
	IP, err := net.InterfaceAddrs()

	//If there is an error return it
	if err != nil {
		log.Fatal(err)
	}

	//Split userIP and retrives only important information
	userIP = IP[1].String()[:len(IP[1].String())-3]

	log.Printf("User IP: %v\n", userIP)

	return userIP
}

func startManagers(userIP string) {

	//Create Data manager, Device manager and Order manager
	managers.DtManager = managers.NewDataManager()
	managers.DvManager = managers.NewDeviceManager(userIP)
	managers.OrdManager = managers.NewOrderManager()

	managers.OrdManager.Start()
}

func startSenders() {

	//Creates a Client that sends data handler's requests
	senders.DHSender = senders.NewDataHandlerClient()

	//Create a Client that sand device's requests
	senders.DvSender = senders.NewDeviceClient()
}
