package main

import (
	"discovery-service/discovery"
)

func main() {
	discovery.Start()
}
