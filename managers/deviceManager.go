package managers

import (
	"discovery-service/datapackage"
	"discovery-service/senders"
	"discovery-service/settings"
	"discovery-service/util"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"sync"
)

var (
	//Wg is an waitgroup that will be use to coordenate device manager's go routines
	wg sync.WaitGroup

	//DvManager is the variable for the Device Manager
	DvManager *DeviceManager
)

//DeviceManager is the entity that take cares about the devices relation
type DeviceManager struct {
	Create            map[string]func() datapackage.DataPackage
	NetworkRange      []string
	NetworkID         string
	Queue             chan []datapackage.DataPackage
	GoRoutinesLimiter chan struct{}
}

//NewDeviceManager creates a new Device Manager
func NewDeviceManager(UserIP string) *DeviceManager {

	//Make a range for network IPs from UserIP
	NtRange := util.MakeTargetsArray(UserIP)

	//Create a Device Manager with a map of functions and a network IPs
	return &DeviceManager{
		Create: map[string]func() datapackage.DataPackage{
			"energy":   datapackage.CreateEnergy,
			"midia":    datapackage.CreateMidia,
			"location": datapackage.CreateLocation,
			"tcp":      datapackage.CreateTCP,
			"http":     datapackage.CreateHTTP,
			"nestin":   datapackage.CreateNestin,
			"mqtt":     datapackage.CreateMQTT,
			"audio":    datapackage.CreateAudio,
		},
		NetworkRange:      NtRange,
		Queue:             make(chan []datapackage.DataPackage, settings.DeviceManagerQueueSize),
		GoRoutinesLimiter: make(chan struct{}, settings.ConnectionSocketSize),
	}
}

//ReturnScenarioID will return the created scenarios IDs to device
func (d *DeviceManager) ReturnScenarioID(object *datapackage.ObjectPayload) (err error) {

	//Make a connection with device and to return created scenariosID
	_, err = senders.DvSender.Send(settings.OperationReturnScenarios, object.DeviceIP, settings.DevicePort, object.Scenarios)

	//If there is an error return it
	if err != nil {
		err = fmt.Errorf("[ERROR] could not return scenarios to device :%v", err)
		return
	}

	log.Printf("[DISCOVERY] %v scenarios returned to Device[%v]", len(object.Scenarios), object.DeviceIP)
	return
}

//Scan sends a TCP request to a host and give the payload to a parse function
func (d *DeviceManager) Scan() {

	log.Printf("[DISCOVERY] Scanning the Network ... ")

	//Catch one host in network range
	for _, host := range d.NetworkRange {

		//Add one goroutine to wait group
		wg.Add(1)

		//Put one goroutine into device goroutines counter
		d.GoRoutinesLimiter <- struct{}{}

		//Starts a connection approach for a host and then try to parse obtained payload
		go func(host1 string) {

			//Ensures that the goroutine added in wait group will be removed
			defer wg.Done()

			//Connect to a host and take the SI
			payload, err := senders.DvSender.Send(settings.OperationIdentify, host1, settings.DevicePort, nil)

			//If there was an error, return and log it
			if err != nil {
				//log.Printf("[ERROR] Could not connect with device : %v", err)

				//Remove this current gorountine from goroutine counter
				<-d.GoRoutinesLimiter
				return

			}

			// Parse the payload to retrive object fill with scenario(s)
			object, err := d.parsePayload(host1, payload)

			//If there was an error, return and log it
			if err != nil {
				log.Printf("[ERROR] Parse error : %v", err)

				//Remove this current gorountine from goroutine counter
				<-d.GoRoutinesLimiter
				return
			}

			DtManager.Register(object)

			//Remove this current gorountine from goroutine counter
			<-d.GoRoutinesLimiter
		}(host)

	}
	//Wait until every host in range was scanned
	wg.Wait()
}

//parsePayload parse the payload and send the information to Data Manager Queue if treat succeed
func (d *DeviceManager) parsePayload(hostIP string, payload []byte) (object *datapackage.ObjectPayload, err error) {

	//Catch chipID and scenario(s) JSON from payload
	payloadJSON, chipID, err := trimPayload(payload)

	//If there is an error return it
	if err != nil {
		return &datapackage.ObjectPayload{}, errors.New("trim error :" + err.Error())
	}

	//Result will be the variable to put unmarshal payload's result
	var result []datapackage.Payload

	//Will be the slice that contains created scenario(s)
	var scenarios []datapackage.DataPackage

	//Unmarshal the payload Scenario slice
	err = json.Unmarshal(payloadJSON, &result)

	//If there is an error return it
	if err != nil {
		err = fmt.Errorf("unmarshal error host[%v]", err)
		return
	}

	//Catch a scenario in of result (Slice of Scenario Struct)
	for _, scenario := range result {

		//Try to unmarshal scenario into a specific struct
		dataPackage, err := d.scenarioUnmarshal(scenario, hostIP, chipID)

		//If there is an error return it
		if err != nil {
			return &datapackage.ObjectPayload{}, fmt.Errorf("scenario unmarshal error : %v", err)
		}

		//Append created scenario to an slice
		scenarios = append(scenarios, dataPackage)
	}

	//Print for Test
	log.Printf("[DISCOVERY] Discovered a Smart-Object - IP: %v - Scenarios: %v\n", hostIP, scenarios[0].ReturnName())

	//Create an object with scenario(s)
	object = &datapackage.ObjectPayload{Instance: OrdManager.OrderStruct.InstanceID, Name: string(chipID), Scenarios: scenarios, DeviceIP: hostIP}

	//Return nil value for error an a fill object
	return
}

//scenarioMarshal Tries to put incomming payload into a specific struct and return it
func (d *DeviceManager) scenarioUnmarshal(scenario datapackage.Payload, hostIP string, chipID []byte) (dataPackage datapackage.DataPackage, err error) {

	//Verifies if scenario is supported
	createFunc, valid := d.Create[scenario.ScenarioType]

	//If is not supported return an error
	if valid != true {
		return nil, errors.New("scenario not supported host[" + hostIP + "]")
	}

	//Executes a function that creates a data package of specified scenario
	dataPackage = createFunc()

	//Tries to unmarshal the payload into created scenario Struct
	err = json.Unmarshal(scenario.ScenarioInfo, dataPackage)

	//If there is an error log and return it
	if err != nil {
		return nil, err
	}

	//log.Printf("SCENARIO NAME:  %v\n", string(scenario.ScenarioInfo))

	//Retrives scenario type to created struct
	dataPackage.RetriveScenarioType(scenario.ScenarioType)

	//Validates unmarshaled struct
	err = settings.Val.Struct(dataPackage)

	//If there is an error log and return it
	if err != nil {
		return nil, err
	}

	return dataPackage, err
}

func trimPayload(payload []byte) (payloadJSON []byte, chipID []byte, err error) {

	//Check payload size, if there is an error return it
	if len(payload) < settings.DiscoveryChipIDSize {
		return nil, nil, errors.New("invalid payload size")
	}

	//Trim payload in order to catch only chiplD part
	chipID = payload[0:settings.DiscoveryChipIDSize]

	//Trim payload in order to catch JSON part of payload
	payloadJSON = payload[settings.DiscoveryChipIDSize:]

	//log.Printf("ChipID: %v | JSON: %v\n", string(chipID), string(payloadJSON))

	return
}
