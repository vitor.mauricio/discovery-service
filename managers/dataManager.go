package managers

import (
	"discovery-service/datapackage"
	"discovery-service/senders"
	"discovery-service/settings"
	"encoding/json"
	"errors"
	"log"
)

var (
	//DtManager is a variable that contains a Data Manager
	DtManager *DataManager
)

//DataManager defines DataManager's struct
type DataManager struct {
	ObjectCount int
}

//NewDataManager will create a new Data Manager entity
func NewDataManager() (d *DataManager) {
	return &DataManager{ObjectCount: 0}
}

//Register will register a new smartObject
func (d *DataManager) Register(object *datapackage.ObjectPayload) {

	//Create and smart object and put his ID on it
	err := d.CreateSmartObject(object)

	//If there is an error log it and return
	if err != nil {
		log.Printf("[ERROR] Could not create a smart object: %v", err)
		return
	}

	//Device manager will return created scenario(s) ID(s) to device
	DvManager.ReturnScenarioID(object)
}

//CreateSmartObject will send a request to Datahandler to create a SmartObject and return the ID
func (d *DataManager) CreateSmartObject(object *datapackage.ObjectPayload) (err error) {

	//Create a JSON to create a smart Object
	payload, err := object.JSON()

	//log.Printf("Payload: %v\n", payload)

	//If there is an error return it
	if err != nil {
		return err
	}

	//Creates a smart object ID, sending a request HTTP to data handler
	DHPayload, err := senders.DHSender.Send(payload, settings.OperationObject, OrdManager.OrderStruct.RequestID)

	//If there is an error log and return it
	if err != nil {
		return err
	}

	//Catch registered scenario(s) ID(s) and retrive it to respective struct
	err = d.DisposeScenarioID(DHPayload, object)

	//If there is an error return it
	if err != nil {
		return err
	}

	log.Printf("[DISCOVERY] A new Smart Object was created: %v\nScenarios:", object.Name)
	for _, scenario := range object.Scenarios {
		log.Printf("[DISCOVERY]%v- ID: %v\n", scenario.ReturnName(), scenario.ReturnScenarioID())
	}

	//Increment one object when it was created on a discovery process
	DtManager.ObjectCount++

	//Returns no error
	return nil
}

//DisposeScenarioID will allocate respective scenario(s) ID(s) to it scenario
func (d *DataManager) DisposeScenarioID(payload []byte, object *datapackage.ObjectPayload) (err error) {

	//Define an array for incoming scenario(s) ID(s)
	var arr []map[string]string

	//Difine an counter that counts retrived scenarios
	var retrived int

	//Try to unmarshal payload into defined array
	err = json.Unmarshal(payload, &arr)

	//If there is an error return it
	if err != nil {
		return err
	}

	//Algorithm to give scenario ID to each scenario from an object struct
	for _, scenario := range object.Scenarios {
		for _, scenarioIDMap := range arr {
			scenarioID, valid := scenarioIDMap[scenario.ReturnName()]

			if valid == true {
				scenario.RetriveScenarioID(scenarioID)
				retrived = retrived + 1
			}
		}
	}

	//If there are possible inconsistencies with registered scenarios return an error
	if len(arr) != retrived {
		return errors.New("[ERROR] inconsistent scenario(s) ID(s)")
	}

	return
}
