package managers

import (
	"discovery-service/senders"
	"discovery-service/settings"
	"encoding/json"
	"log"
	"time"
)

var (
	//OrdManager is the variable for the Order Manager
	OrdManager *OrderManager
)

//OrderManager will manage when discovery starts
type OrderManager struct {
	OrderID chan string

	//The struct contains instanceID and requestID for discovery
	OrderStruct *senders.OrderPayload
}

//NewOrderManager will instanciate a new Order Manager
func NewOrderManager() *OrderManager {
	return &OrderManager{OrderID: make(chan string, 1), OrderStruct: senders.CreateOrderPayload()}
}

//Start order manager
func (o *OrderManager) Start() {

	log.Println("[DISCOVERY] Discovery is running")
	for {

		//Send an request to data handler to figure if there is an pending discovery order request
		DHPayload, err := senders.DHSender.Send(settings.EmptyString, settings.OperationDiscovery, settings.EmptyString)

		//If there is an error, log it and then wait a little time to scan again
		if err != nil {
			log.Printf("[ERROR] discovery order fail: %v", err)
			time.Sleep(settings.DiscoveryPollingTime * time.Second)
			continue
		}

		if err = json.Unmarshal(DHPayload, &o.OrderStruct); err != nil {
			log.Printf("[ERROR] discovery order fail: %v", err)
			time.Sleep(settings.DiscoveryPollingTime * time.Second)
			continue
		}

		if err = o.OrderStruct.Validate(); err != nil {
			log.Printf("[ERROR] discovery order fail: %v", err)
			time.Sleep(settings.DiscoveryPollingTime * time.Second)
			continue
		}

		log.Printf("[DISCOVERY] A request has arrived ")

		//Start discovery process by scanning local network
		DvManager.Scan()

		//At the end of scan, send an request notifying data handler
		_, err = senders.DHSender.Send(o.doneJSON(), settings.OperationDone, settings.EmptyString)

		if err != nil {
			log.Printf("[DISCOVERY] Done request fail : %v ", err)
		}

		log.Printf("[DISCOVERY] Scan was completed ")

		log.Printf("[DISCOVERY] Discovered %v smart Objects ", DtManager.ObjectCount)

		//Resets object count when discovery process is over
		DtManager.ObjectCount = 0
	}
}
func (o *OrderManager) doneJSON() (orderJSON string) {

	//Put into a struct to make a marshal
	requestJSON := senders.RequestID{RequestID: o.OrderStruct.RequestID}

	//Transform instanceID struct into an byte array
	orderByte, err := json.Marshal(&requestJSON)

	//If there is an error return it
	if err != nil {
		return
	}

	//Transform previous byte array (instanceID JSON) into a string
	orderJSON = string(orderByte)

	log.Printf("JSON DONE: %v\n", orderJSON)

	return

}
