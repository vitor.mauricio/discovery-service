package datapackage

//DataScenarioMidia is the struct that defines the scenario
type DataScenarioMidia struct {
	Name       string `json:"name" validate:"required"`
	Format     string `json:"format" validate:"required,oneof=json byte xml"`
	Type       string `json:"type"`
	ScenarioID string `json:"-"`
}

//CreateMidia creates a struct for Midia Scenario
func CreateMidia() DataPackage {
	return &DataScenarioMidia{}
}

//ReturnName returns scenario Name
func (d *DataScenarioMidia) ReturnName() (name string) {
	return d.Name
}

//RetriveScenarioID retrives an ScenarioID
func (d *DataScenarioMidia) RetriveScenarioID(s string) {
	d.ScenarioID = s
}

//RetriveScenarioType retrives the type of scenario
func (d *DataScenarioMidia) RetriveScenarioType(t string) {
	d.Type = t
}

//ReturnScenarioID retives the ScenarioID of a Scenario
func (d *DataScenarioMidia) ReturnScenarioID() string {
	return "02" + d.ScenarioID
}
