package datapackage

import (
	"encoding/json"
)

//ObjectPayload is a object struct that will be marshaled and send to data handler sender
type ObjectPayload struct {
	DeviceIP  string        `json:"-"`
	Instance  string        `json:"instance_id"`
	Name      string        `json:"name"`
	Scenarios []DataPackage `json:"scenarios"`
}

//JSON A function that transform in JSON the ObjectPayload, that will be sended to data handler
func (o *ObjectPayload) JSON() (JSON string, err error) {
	json, err := json.Marshal(o)
	if err != nil {
		return "", err
	}
	return string(json), nil
}

//Payload abstract struct that will be unsmarshaled
type Payload struct {
	ScenarioType string          `json:"type" `
	ScenarioInfo json.RawMessage `json:"configs" `
}

//DataPackage is an interface that defines functions that every Scenario must have
type DataPackage interface {
	RetriveScenarioID(string)

	RetriveScenarioType(string)

	ReturnName() string

	ReturnScenarioID() string
}
