package datapackage

//DataScenarioMQTT is the struct that defines the scenario
type DataScenarioMQTT struct {
	Name       string `json:"name" validate:"required"`
	Format     string `json:"format" validate:"required,oneof=json byte xml"`
	Endpoint   string `json:"endpoint" validate:"required,url"`
	Topic      string `json:"topic"`
	UserName   string `json:"username"`
	Password   string `json:"password"`
	ScenarioID string `json:"-"`
	Type       string `json:"type"`
}

//CreateMQTT creates a struct for MQTT Routing Scenario
func CreateMQTT() DataPackage {
	return &DataScenarioMQTT{}
}

//ReturnName returns scenario Name
func (d *DataScenarioMQTT) ReturnName() (name string) {
	return d.Name
}

//RetriveScenarioID retrives an ScenarioID
func (d *DataScenarioMQTT) RetriveScenarioID(s string) {
	d.ScenarioID = s
}

//RetriveScenarioType retrives the type of scenario
func (d *DataScenarioMQTT) RetriveScenarioType(t string) {
	d.Type = t
}

//ReturnScenarioID retives the ScenarioID of a Scenario
func (d *DataScenarioMQTT) ReturnScenarioID() string {
	return "06" + d.ScenarioID
}
