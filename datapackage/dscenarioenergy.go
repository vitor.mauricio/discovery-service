package datapackage

//DataScenarioEnergy is the struct that defines the scenario
type DataScenarioEnergy struct {
	Name       string `json:"name" validate:"required"`
	Format     string `json:"format" validate:"required,oneof=json byte xml"`
	Type       string `json:"type" validate:"required"`
	ScenarioID string `json:"-"`
}

//CreateEnergy creates a struct for Energy Scenario
func CreateEnergy() DataPackage {
	return &DataScenarioEnergy{}
}

//ReturnName returns scenario Name
func (d *DataScenarioEnergy) ReturnName() (name string) {
	return d.Name
}

//RetriveScenarioID retrives an ScenarioID
func (d *DataScenarioEnergy) RetriveScenarioID(s string) {
	d.ScenarioID = s
}

//RetriveScenarioType retrives the type of scenario
func (d *DataScenarioEnergy) RetriveScenarioType(t string) {
	d.Type = t
}

//ReturnScenarioID retrives the ScenarioID of a Scenario
func (d *DataScenarioEnergy) ReturnScenarioID() string {
	return "01" + d.ScenarioID
}
