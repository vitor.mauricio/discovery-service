package datapackage

//DataScenarioNestin is the struct that defines the scenario
type DataScenarioNestin struct {
	Name            string            `json:"name" validate:"required"`
	Format          string            `json:"format" validate:"required,oneof=json byte xml"`
	Endpoint        string            `json:"endpoint" validate:"required,url"`
	Method          string            `json:"method" validate:"required"`
	Query           string            `json:"query"`
	MinRequestSize  int               `json:"min_request_size"`
	MaxResponseSize int               `json:"max_response_size"`
	Headers         map[string]string `json:"headers"`
	ScenarioID      string            `json:"-"`
	Type            string            `json:"type"`
}

//CreateNestin creates a struct for Nestin Routing Scenario
func CreateNestin() DataPackage {
	return &DataScenarioNestin{}
}

//ReturnName returns scenario Name
func (d *DataScenarioNestin) ReturnName() (name string) {
	return d.Name
}

//RetriveScenarioID retrives an ScenarioID
func (d *DataScenarioNestin) RetriveScenarioID(s string) {
	d.ScenarioID = s
}

//RetriveScenarioType retrives the type of scenario
func (d *DataScenarioNestin) RetriveScenarioType(t string) {
	d.Type = t
}

//ReturnScenarioID retives the ScenarioID of a Scenario
func (d *DataScenarioNestin) ReturnScenarioID() string {
	return "07" + d.ScenarioID
}
