package datapackage

//DataScenarioLocation is the struct that defines the scenario
type DataScenarioLocation struct {
	Name       string `json:"name" validate:"required"`
	Format     string `json:"format" validate:"required,oneof=json byte xml"`
	Type       string `json:"type"`
	ScenarioID string `json:"-"`
}

//CreateLocation creates a struct for Location Scenario
func CreateLocation() DataPackage {
	return &DataScenarioLocation{}
}

//ReturnName returns scenario Name
func (d *DataScenarioLocation) ReturnName() (name string) {
	return d.Name
}

//RetriveScenarioID retrives an ScenarioID
func (d *DataScenarioLocation) RetriveScenarioID(s string) {
	d.ScenarioID = s
}

//RetriveScenarioType retrives the type of scenario
func (d *DataScenarioLocation) RetriveScenarioType(t string) {
	d.Type = t
}

//ReturnScenarioID retives the ScenarioID of a Scenario
func (d *DataScenarioLocation) ReturnScenarioID() string {
	return "03" + d.ScenarioID
}
