package datapackage

//DataScenarioAudio is the struct that defines the scenario
type DataScenarioAudio struct {
	Name       string `json:"name" validate:"required"`
	Format     string `json:"format" validate:"required,oneof=json byte xml"`
	Type       string `json:"type"`
	ScenarioID string `json:"-"`
}

//CreateAudio creates a struct for Energy Scenario
func CreateAudio() DataPackage {
	return &DataScenarioAudio{}
}

//ReturnName returns scenario Name
func (d *DataScenarioAudio) ReturnName() (name string) {
	return d.Name
}

//RetriveScenarioID retrives an ScenarioID
func (d *DataScenarioAudio) RetriveScenarioID(s string) {
	d.ScenarioID = s
}

//RetriveScenarioType retrives the type of scenario
func (d *DataScenarioAudio) RetriveScenarioType(t string) {
	d.Type = t
}

//ReturnScenarioID retrives the ScenarioID of a Scenario
func (d *DataScenarioAudio) ReturnScenarioID() string {
	return "08" + d.ScenarioID
}
