package datapackage

//DataScenarioTCP is the struct that defines the scenario
type DataScenarioTCP struct {
	Name         string `json:"name" validate:"required"`
	Format       string `json:"format" validate:"required,oneof=json byte xml"`
	Endpoint     string `json:"endpoint" validate:"required,url"`
	Timeout      int    `json:"timeout"`
	ResponseSize int    `json:"response_size"`
	ScenarioID   string `json:"-"`
	Type         string `json:"type"`
}

//CreateTCP creates a struct for TCP Routing Scenario
func CreateTCP() DataPackage {
	return &DataScenarioTCP{}
}

//ReturnName returns scenario Name
func (d *DataScenarioTCP) ReturnName() (name string) {
	return d.Name
}

//RetriveScenarioID retrives an ScenarioID
func (d *DataScenarioTCP) RetriveScenarioID(s string) {
	d.ScenarioID = s
}

//RetriveScenarioType retrives the type of scenario
func (d *DataScenarioTCP) RetriveScenarioType(t string) {
	d.Type = t
}

//ReturnScenarioID retives the ScenarioID of a Scenario
func (d *DataScenarioTCP) ReturnScenarioID() string {
	return "05" + d.ScenarioID
}
