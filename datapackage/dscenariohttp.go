package datapackage

//DataScenarioHTTP is the struct that defines the scenario
type DataScenarioHTTP struct {
	Name       string            `json:"name" validate:"required"`
	Format     string            `json:"format" validate:"required,oneof=json byte xml"`
	Endpoint   string            `json:"endpoint" validate:"required,url"`
	Method     string            `json:"method" validate:"required"`
	Query      string            `json:"query"`
	Headers    map[string]string `json:"headers"`
	ScenarioID string            `json:"-"`
	Type       string            `json:"type"`
}

//CreateHTTP creates a struct for HTTP Routing Scenario
func CreateHTTP() DataPackage {
	return &DataScenarioHTTP{Headers: make(map[string]string)}
}

//ReturnName returns scenario Name
func (d *DataScenarioHTTP) ReturnName() (name string) {
	return d.Name
}

//RetriveScenarioID retrives an ScenarioID
func (d *DataScenarioHTTP) RetriveScenarioID(s string) {
	d.ScenarioID = s
}

//RetriveScenarioType retrives the type of scenario
func (d *DataScenarioHTTP) RetriveScenarioType(t string) {
	d.Type = t
}

//ReturnScenarioID retives the ScenarioID of a Scenario
func (d *DataScenarioHTTP) ReturnScenarioID() string {
	return "04" + d.ScenarioID
}
