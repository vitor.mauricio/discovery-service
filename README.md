# Discovery Service

This is a Discovery Service to discover smart objects in a heterogeneous network. This service will support a device management for a network that use the MiddleWay of Citara.

# Utilização

Flags:
* --networkid: NetworkID que o discovery usará para cadastrar os devices encontrados.
