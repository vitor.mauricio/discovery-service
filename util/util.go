package util

import (
	"discovery-service/settings"
	"strconv"
	"strings"
)

//MakeTargetsArray Make a slice with network IPs range
func MakeTargetsArray(userIP string) []string {
	defaut := strings.Split(userIP, ".")
	front := defaut[0] + "." + defaut[1]
	ranges, _ := strconv.Atoi(defaut[2])
	var targets []string
	for i := ranges - settings.NetworkScanRange; i < ranges+settings.NetworkScanRange; i++ {
		result := front + "." + strconv.Itoa(i)
		for i := 0; i < 255; i++ {
			targets = append(targets, result+"."+strconv.Itoa(i))
		}
	}
	return targets

}
